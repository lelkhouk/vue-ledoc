import { createRouter, createWebHashHistory } from 'vue-router'
import Login from "./pages/Login.vue"
import Dashboard from "./pages/Dashboard.vue"
import SignIn from "./pages/Sign-in.vue"
import Patient from "./pages/Patient.vue"

export const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard
        },
        {
            path:'/signIn',
            name : 'SignIn',
            component : SignIn
        },
        {
            path:'/patient',
            name : 'Patient',
            component : Patient
        },
        {
            path:'/login',
            name : 'Login',
            component : Login
        },
        {
            path:'/logout',
            name : 'Logout'
        },
    ]
});